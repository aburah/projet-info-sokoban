#include "Case.h"
#include "maze.h"
#include "utils/coord.h"

Case::Case()
{
    //ctor
}

Case::~Case()
{
    //dtor
}

unsigned short Case::getOrdre()
{
    return (m_ordre);
}
void Case::setOrdre(unsigned short ordre)
{
    m_ordre=ordre;
}
unsigned short Case::getDeplacement()
{
    return (m_deplacement);
}
void Case::setDeplacement(unsigned short deplacement)
{
    m_deplacement=deplacement;
}
unsigned short Case::getPrecedent()
{
    return (m_precedent);
}
void Case::setPrecedent(unsigned short precedent)
{
    m_precedent=precedent;
}
unsigned short Case::getPosCase()
{
    return (m_pos_case);
}
void Case::setPosCase(unsigned short pos_case)
{
    m_pos_case=pos_case;
}
bool Case::getZonePerso()
{
    return m_zone_pesonnage;
}
void Case::setZonePerso(bool zone_personnage)
{
    m_zone_pesonnage = zone_personnage;
}
unsigned short Case::getType()
{
    return (m_type);
}
void Case::setType(unsigned short type)
{
    m_type=type;
}
bool Case::getisIn()
{
    return (m_isIn);
}
void Case::setisIn(bool isIn)
{
    m_isIn=isIn;
}

bool Case::isWalkable()
{
    if ((this->getType() == 0) || (this->getType() == 4) || (this->getType() == 9))
    {
        return true;
    }
    else
        return false;
}

