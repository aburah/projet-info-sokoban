#ifndef CASE_H
#define CASE_H


class Case
{
    public:
        Case();
        virtual ~Case();
        unsigned short getPosCase();
        void setPosCase(unsigned short pos_case);
        unsigned short getOrdre();
        void setOrdre(unsigned short ordre);
        unsigned short getDeplacement();
        void setDeplacement(unsigned short deplacement);
        unsigned short getPrecedent();
        void setPrecedent(unsigned short precedent);
        bool getZonePerso();
        void setZonePerso(bool zone_personnage);
        unsigned short getType();
        void setType(unsigned short type);
        bool getisIn();
        void setisIn(bool isIN);

        bool isWalkable();



    protected:
    private:
        unsigned short m_pos_case;
        unsigned short m_type;
        unsigned short m_ordre;
        unsigned short m_deplacement;
        unsigned short m_precedent;
        bool m_zone_pesonnage;
        bool m_isIn;

};

#endif // CASE_H
