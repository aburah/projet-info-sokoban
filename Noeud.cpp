#include "Noeud.h"
#include "utils/coord.h"
#include "utils/console.h"
#include <queue>
#include <iostream>

Noeud::Noeud()
{
    //ctor
}

Noeud::~Noeud()
{
    //dtor
}

unsigned short Noeud::getNumber()
{
    return (m_number);
}
void Noeud::setNumber(unsigned short number)
{
    m_number=number;
}
unsigned short Noeud::getPreced()
{
    return (m_precednumber);
}
void Noeud::setPreced(unsigned short preced)
{
    m_precednumber=preced;
}
unsigned short Noeud::getPosPlayer()
{
    return (m_playerpos);
}
void Noeud::setPosPlayer(unsigned short playerpos)
{
    m_playerpos=playerpos;
}



void Noeud::zoneConnexeN()
{
    std::queue <Case> file;
    for (unsigned short int i=0; i<m_fieldN.size(); i++)
    {
        m_fieldN[i].setZonePerso(false);
    }
    for (unsigned short i=0; i < m_fieldN.size(); i++)
    {
        if(m_fieldN[i].getPosCase() == m_playerpos)
        {
            file.push(m_fieldN[i]);
            m_fieldN[i].setZonePerso(true);
        }
    }
    while (!file.empty())
    {
        for (int i=0; i<4; i++)
        {
            int pos = Coord::getDirPos(file.front().getPosCase(), i);
            if ((m_fieldN[pos].isWalkable()) && (!m_fieldN[pos].getZonePerso()))
            {
                file.push(m_fieldN[pos]);
                m_fieldN[pos].setZonePerso(true);
            }
        }
        file.pop();
    }
    for (unsigned short i=0; i < m_fieldN.size(); i++)
    {
        //std::cout << "Connexe : " << m_fieldN[i].getZonePerso() << std::endl;
    }
}



bool Noeud::isCompleted()
{
    for (unsigned int i=0; i<this->m_pos_boxes.size(); ++i)
    {
        if (m_fieldN[m_pos_boxes[i]].getType() != 3)
            return false;
    }
    return true;
}


void Noeud::affichageNoeud()
{
    for(int i=0; i < m_fieldN.size(); i++)
    {
        if (m_fieldN[i].getType()==1) Console::getInstance()->setColor(_COLOR_PURPLE);
        else if (m_fieldN[i].getType()==4 || m_fieldN[i].getType()==3) Console::getInstance()->setColor(_COLOR_GREEN);
        else if (m_fieldN[i].getType()==2) Console::getInstance()->setColor(_COLOR_BLUE);
        else if (m_fieldN[i].getType()==9) Console::getInstance()->setColor(_COLOR_RED);
        else Console::getInstance()->setColor(_COLOR_WHITE);

        if(m_fieldN[i].getPosCase() == m_playerpos) Console::getInstance()->setColor(_COLOR_YELLOW);

        if (i%Coord::m_nb_col == 0)
            std::cout << std::endl;
        std::cout << m_fieldN[i].getType() << " ";
        Console::getInstance()->setColor(_COLOR_WHITE);

    }
    std::cout << std::endl;

}
