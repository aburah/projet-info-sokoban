#ifndef NOEUD_H
#define NOEUD_H
#include "Case.h"
#include <vector>

class Noeud
{
    public:
        Noeud();
        virtual ~Noeud();
        unsigned short getNumber();
        void setNumber(unsigned short number);
        unsigned short getPreced();
        void setPreced(unsigned short preced);
        void zoneConnexeN();
        unsigned short getPosPlayer();
        void setPosPlayer(unsigned short posplayer);

        bool isCompleted();
        void affichageNoeud();

        std::vector <Case> m_fieldN;
        std::vector<unsigned short> m_pos_boxes;



    protected:
    private:
        int m_number, m_precednumber, m_playerpos;

};

#endif // NOEUD_H
