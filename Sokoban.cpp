/************************************************************
Sokoban project - Maze file
Copyright Florent DIEDLER
Date : 27/02/2016

Please do not remove this header, if you use this file !
************************************************************/

#include "maze.h"
#include "graphic.h"
#include "utils/console.h"
#include "utils/coord.h"
#include <fstream>
#include <iomanip>
#include <queue>
#include <stack>
#include <unistd.h>
#include <winalleg.h>


Maze::Maze(const std::string& path)
    : m_lig(0), m_col(0), m_pos_player(0), m_dir_player(TOP), m_level_path(path)
{
}

Maze::~Maze()
{
}



void Maze::infosCases()
{
    for (unsigned int i = 0; i < m_cases.size(); i++)
    {
        if (m_cases[i].getType()==SPRITE_WALL) Console::getInstance()->setColor(_COLOR_PURPLE);
        else if (m_cases[i].getType()==SPRITE_GOAL || m_cases[i].getType()==SPRITE_BOX_PLACED) Console::getInstance()->setColor(_COLOR_GREEN);
        else if (m_cases[i].getType()==SPRITE_BOX) Console::getInstance()->setColor(_COLOR_BLUE);
        else if (m_cases[i].getType()==SPRITE_DEADSQUARE) Console::getInstance()->setColor(_COLOR_RED);
        else Console::getInstance()->setColor(_COLOR_WHITE);

        if (i%Coord::m_nb_col == 0)
            std::cout << std::endl;
        std::cout << m_cases[i].getType() << " ";
        Console::getInstance()->setColor(_COLOR_WHITE);


    }
}


bool Maze::init()
{
    bool res = this->_load(this->m_level_path);
    if (!res)
    {
        std::cerr << "Cannot load maze... Check file : " << this->m_level_path << std::endl;
        return false;
    }

    return res;
}

void Maze::setPlayerDir(char dir)
{
    m_dir_player = dir;
}


// Check if all boxes are on a goal
bool Maze::isCompleted() const
{
    for (unsigned int i=0; i<this->m_pos_boxes.size(); ++i)
    {
        if (!this->isSquareBoxPlaced(this->m_pos_boxes[i]))
            return false;
    }
    return true;
}


// Check if we can push a box in a direction
// INPUT: position of the box to check, direction,
// OUTPUT : the position of the box after pushing
//      TRUE if all goes right otherwise FALSE
bool Maze::_canPushBox(unsigned short posBox, char dir, unsigned short& newPosBox) const
{
    // Check if this position is a box !
    if (!this->isSquareBox(posBox))
        return false;

    // Compute new position according to push direction
    newPosBox = Coord::getDirPos(posBox, dir);

    // Can we push the box ?
    return this->isSquareWalkable(newPosBox);
}

// Load a maze from a file (DO NOT TOUCH)
bool Maze::_load(const std::string& path)
{
    std::vector<unsigned short> tmpPosBoxes;
    std::ifstream ifs(path.c_str());
    if (ifs)
    {
        std::vector<std::string> lines;
        std::string line;
        while (std::getline(ifs, line))
        {
            lines.push_back(line);
            this->m_lig++;
            this->m_col = (this->m_col < line.size() ? line.size() : this->m_col);
        }
        ifs.close();

        if (this->m_col > NB_MAX_WIDTH || this->m_lig > NB_MAX_HEIGHT)
        {
            std::cerr << "Maze::load => Bad formatting in level data..." << std::endl;
            return false;
        }

        Coord::m_nb_col = this->m_col;
        for (unsigned int i=0; i<lines.size(); i++)
        {
//            LDebug << "Maze::load => Reading : " << lines[i];
            for (unsigned int j=0; j<this->m_col; j++)
            {
                if (j < lines[i].size())
                {
                    bool both = false;
                    unsigned short pos = Coord::coord1D(i, j);
                    unsigned char s = (unsigned char)(lines[i][j] - '0');

                    // Need to add a goal and a box ;)
                    if (s == SPRITE_BOX_PLACED)
                    {
                       both = true;
                    }

                    if (s == SPRITE_GOAL || both)
                    {
                        this->m_pos_goals.push_back(pos);
                    }
                    if (s == SPRITE_BOX || both)
                    {
                        tmpPosBoxes.push_back(pos);
                    }

                    // Assign player position
                    if (s == SPRITE_MARIO)
                    {
                        this->m_pos_player = pos;
                        //LDebug << "\tAdding player pos (" << pos << ")";
                        s = SPRITE_GROUND;
                    }

                    // Add this value in the field
                    this->m_field.push_back(s);
                }
                else
                {
                    // Here - Out of bound
                    this->m_field.push_back(SPRITE_OUT);
                }
            }
        }

        // Copy box position
        this->m_pos_boxes.resize(tmpPosBoxes.size());
        for (unsigned int i=0; i<tmpPosBoxes.size(); ++i)
        {
            this->m_pos_boxes[i] = tmpPosBoxes[i];
        }

        return (this->m_pos_boxes.size() == this->m_pos_goals.size());
    }
    else
    {
        std::cerr << "Maze::load => File does not exist..." << std::endl;
    }
    return false;
}

void Maze::del()
{
    m_lig=0;
    m_col=0;
    m_field.clear();
    m_pos_boxes.clear();
    m_pos_goals.clear();
}

void Maze::allocNoeuds()
{
    int j=0;
    m_etats.resize(1);
    m_etats[0].m_fieldN.resize(m_field.size());
    m_etats[0].m_pos_boxes.resize(m_pos_boxes.size());
    m_etats[0].setPosPlayer(m_pos_player);
    m_etats[0].setNumber(1);
    m_etats[0].setNumber(0);


    for (unsigned int i=0; i < m_field.size(); i++)
    {
        m_etats[0].m_fieldN[i].setPosCase(i);
        m_etats[0].m_fieldN[i].setOrdre(countOrder(i));
        m_etats[0].m_fieldN[i].setType(m_field[i]);
        if ((m_cases[i].getType() == SPRITE_BOX) || (m_cases[i].getType() == SPRITE_BOX_PLACED))
        {
            m_etats[0].m_pos_boxes[j]=i;
            j++;
        }
    }
    m_etats[0].zoneConnexeN();
}

bool Maze::noeudExist(Noeud noeud)
{
    bool identique, zoneidentique;
    for (unsigned int i=0; i < m_etats.size(); i++)
    {
        identique = true;
        zoneidentique = true;
        for (unsigned int j=0; j < noeud.m_fieldN.size(); j++)
        {
            if (noeud.m_fieldN[j].getType() != m_etats[i].m_fieldN[j].getType())
            {
                 identique = false;
            }
        }
        for (unsigned int j=0; j < noeud.m_fieldN.size(); j++)
        {
            if (noeud.m_fieldN[j].getZonePerso() != m_etats[i].m_fieldN[j].getZonePerso())
            {
                 zoneidentique = false;
            }
        }
        if (identique && zoneidentique)
        {
                return true;
        }
    }
    return false;
}


std::queue<Noeud> Maze::newNoeud(std::queue <Noeud> file)
{
    bool goal=false;
    Noeud noeudtop = file.front();
    noeudtop.zoneConnexeN();
    noeudtop.affichageNoeud();
    //system("pause");
    std::cout << "noeud: " << noeudtop.getNumber() <<std::endl;
    for (unsigned int i=0; i < m_pos_boxes.size(); i++)
    {

        std::cout << "Caisse : " << i << std::endl;
        for (unsigned int j=0; j < 4; j++)
        {
            unsigned short pos = Coord::getDirPos(noeudtop.m_pos_boxes[i], j);
            unsigned short posOpposite = Coord::getOppositeDirPos(noeudtop.m_pos_boxes[i], j);
            std::cout << noeudtop.m_fieldN[pos].getType() << "  -  ";

            if (((noeudtop.m_fieldN[pos].getType() == SPRITE_GROUND) || (noeudtop.m_fieldN[pos].getType() == SPRITE_GOAL)) && (noeudtop.m_fieldN[posOpposite].getZonePerso()))
            {
                std::cout << "New Noeud" << std::endl;
                Noeud newnoeud = noeudtop;
                newnoeud.setNumber(m_etats.size());
                newnoeud.setPreced(noeudtop.getNumber());
                    std::cout << "Noeud: " << m_etats.size() << std::endl;

                if (noeudtop.m_fieldN[pos].getType() == SPRITE_GROUND)
                {
                    newnoeud.m_fieldN[pos].setType(SPRITE_BOX);
                }
                else if (noeudtop.m_fieldN[pos].getType() == SPRITE_GOAL)
                {
                    newnoeud.m_fieldN[pos].setType(SPRITE_BOX_PLACED);
                }
                for (unsigned int k=0; k < newnoeud.m_pos_boxes.size(); k++)
                {
                    if (newnoeud.m_pos_boxes[i] == m_pos_goals[k])
                    {
                        goal = true;
                    }
                }
                if (goal)
                    newnoeud.m_fieldN[newnoeud.m_pos_boxes[i]].setType(SPRITE_GOAL);
                else
                    newnoeud.m_fieldN[newnoeud.m_pos_boxes[i]].setType(SPRITE_GROUND);

                goal = false;
                newnoeud.setPosPlayer(newnoeud.m_pos_boxes[i]);
                newnoeud.m_pos_boxes[i]=pos;

                newnoeud.zoneConnexeN();
                ///Test Noeud identique
                if (!noeudExist(newnoeud))
                {
                    file.push(newnoeud);
                    m_etats.push_back(newnoeud);

                }
                else
                    std::cout <<"exist" << std::endl;

            }
        }

    }
    return file;
}

std::stack<Noeud> Maze::newNoeudP(std::stack <Noeud> pile)
{
    bool goal=false;
    Noeud noeudtop = pile.top();
    noeudtop.zoneConnexeN();
    noeudtop.affichageNoeud();
    //system("pause");
    std::cout << "noeud: " << noeudtop.getNumber() <<std::endl;
    for (unsigned int i=0; i < m_pos_boxes.size(); i++)
    {

        std::cout << "Caisse : " << i << std::endl;
        for (unsigned int j=0; j < 4; j++)
        {
            unsigned short pos = Coord::getDirPos(noeudtop.m_pos_boxes[i], j);
            unsigned short posOpposite = Coord::getOppositeDirPos(noeudtop.m_pos_boxes[i], j);
            std::cout << noeudtop.m_fieldN[pos].getType() << "  -  ";

            if (((noeudtop.m_fieldN[pos].getType() == SPRITE_GROUND) || (noeudtop.m_fieldN[pos].getType() == SPRITE_GOAL)) && (noeudtop.m_fieldN[posOpposite].getZonePerso()))
            {
                std::cout << "New Noeud" << std::endl;
                Noeud newnoeud = noeudtop;
                newnoeud.setNumber(m_etats.size());
                newnoeud.setPreced(noeudtop.getNumber());
                    std::cout << "Noeud: " << m_etats.size() << std::endl;

                if (noeudtop.m_fieldN[pos].getType() == SPRITE_GROUND)
                {
                    newnoeud.m_fieldN[pos].setType(SPRITE_BOX);
                }
                else if (noeudtop.m_fieldN[pos].getType() == SPRITE_GOAL)
                {
                    newnoeud.m_fieldN[pos].setType(SPRITE_BOX_PLACED);
                }
                for (unsigned int k=0; k < newnoeud.m_pos_boxes.size(); k++)
                {
                    if (newnoeud.m_pos_boxes[i] == m_pos_goals[k])
                    {
                        goal = true;
                    }
                }
                if (goal)
                    newnoeud.m_fieldN[newnoeud.m_pos_boxes[i]].setType(SPRITE_GOAL);
                else
                    newnoeud.m_fieldN[newnoeud.m_pos_boxes[i]].setType(SPRITE_GROUND);

                goal = false;
                newnoeud.setPosPlayer(newnoeud.m_pos_boxes[i]);
                newnoeud.m_pos_boxes[i]=pos;

                newnoeud.zoneConnexeN();
                ///Test Noeud identique
                if (!noeudExist(newnoeud))
                {
                    pile.push(newnoeud);
                    m_etats.push_back(newnoeud);

                }
                else
                    std::cout <<"exist" << std::endl;

            }
        }

    }
    return pile;
}


void Maze::calculCheminSolution(std::vector<Noeud> cheminN)
{
    int pos, posTOP, posBOTTOM, posLEFT, posRIGHT;
    std::vector<int> bouge;
    ///Chemin Noeuds --> Chemin Perso
    for (unsigned int i=0; i < cheminN.size()-1; i++)
    {
        for (unsigned int j=0;j < cheminN[i].m_pos_boxes.size(); j++)
        {
            if (cheminN[i].m_pos_boxes[j] != cheminN[i+1].m_pos_boxes[j])
            {
                posTOP=Coord::getDirPos(cheminN[i].m_pos_boxes[j], TOP);
                posBOTTOM=Coord::getDirPos(cheminN[i].m_pos_boxes[j], BOTTOM);
                posLEFT=Coord::getDirPos(cheminN[i].m_pos_boxes[j], LEFT);
                posRIGHT=Coord::getDirPos(cheminN[i].m_pos_boxes[j], RIGHT);

                if (posTOP == cheminN[i+1].m_pos_boxes[j])
                {
                    bouge = chemin(cheminN[i], posBOTTOM);
                    if (bouge[0] != 5)
                        for (unsigned int k=0; k < bouge.size(); k++){ m_cheminSolution.push_back(bouge[k]);}
                    m_cheminSolution.push_back(TOP);
                }
                else if (posBOTTOM == cheminN[i+1].m_pos_boxes[j])
                {
                    bouge = chemin(cheminN[i], posTOP);
                    if (bouge[0] != 5)
                        for (unsigned int k=0; k < bouge.size(); k++){ m_cheminSolution.push_back(bouge[k]);}
                    m_cheminSolution.push_back(BOTTOM);
                }
                else if (posLEFT == cheminN[i+1].m_pos_boxes[j])
                {
                    bouge = chemin(cheminN[i], posRIGHT);
                    if (bouge[0] != 5)
                        for (unsigned int k=0; k < bouge.size(); k++){ m_cheminSolution.push_back(bouge[k]);}
                    m_cheminSolution.push_back(LEFT);
                }
                else if (posRIGHT == cheminN[i+1].m_pos_boxes[j])
                {
                    bouge = chemin(cheminN[i], posLEFT);
                    if (bouge[0] != 5)
                        for (unsigned int k=0; k < bouge.size(); k++){ m_cheminSolution.push_back(bouge[k]);}
                    m_cheminSolution.push_back(RIGHT);
                }
            }
        }
    }
    std::cout << std::endl << "Deplacements: " << std::endl;
    for (unsigned int i=0; i < m_cheminSolution.size(); i++)
    {
        std::cout << m_cheminSolution[i] << " - ";
    }
}

void Maze::affichageCheminSolution(Graphic g)
{
    bool win;
    g.clear();
    draw(g);
    g.display(Coord::m_nb_col);
    for (int i; i<m_cheminSolution.size(); i++)
    {

        win = updatePlayer(m_cheminSolution[i]);
        g.clear();
        draw(g);
        g.display(Coord::m_nb_col);
        Sleep(100);
    }

}


unsigned short Maze::countOrder(unsigned short pos_case)
{
    unsigned short ordre=0, lig = (pos_case / m_lig), col = (pos_case % m_col);
    unsigned short pos;
    if ((lig > 1)&& (lig < m_lig) && (col > 1) && (col < m_col))
    {
        for (int i=0; i <4; i++)
        {
            pos=Coord::getDirPos(pos_case, i);
            if (m_field[pos] != SPRITE_WALL)
            {
                ordre++;
            }
        }
    }
    return (ordre);
}



void Maze::alloc_case()
{
    m_cases.resize(m_field.size());
    std::cout << "SIIIZE : " << m_field.size() << std::endl;
    for (unsigned short i=0; i < m_cases.size(); i++)
    {
        m_cases[i].setPosCase(i);
        std::cout << "i: " << i << " - " << std::endl;
        m_cases[i].setOrdre(countOrder(i));
        m_cases[i].setType(m_field[i]);
    }
}


std::vector<int> Maze::chemin(Noeud ini, int dest)
{
    Noeud n = ini;
    //n.affichageNoeud();
    int pos, posTOP, posBOTTOM, posLEFT, posRIGHT;
    std::queue <Case> file;
    std::vector <int> cheminS, cheminSI, cheminDep, cheminDepI;
    int precedent;
    if (dest==n.getPosPlayer())
    {
        cheminS.push_back(5);
        return cheminS;
    }
    int k=0;
    for (unsigned short int i=0; i<n.m_fieldN.size(); i++)
    {
        n.m_fieldN[i].setZonePerso(false);
    }
    for (unsigned short i=0; i < n.m_fieldN.size(); i++)
    {
        if(n.m_fieldN[i].getPosCase() == n.getPosPlayer())
        {
            file.push(n.m_fieldN[i]);
            n.m_fieldN[i].setZonePerso(true);
            n.m_fieldN[i].setPrecedent(0);
        }
    }
    while (!file.empty())
    {
        for (int i=0; i<4; i++)
        {
            pos = Coord::getDirPos(file.front().getPosCase(), i);

            if ((n.m_fieldN[pos].isWalkable()) && (!n.m_fieldN[pos].getZonePerso()))
            {
                file.push(n.m_fieldN[pos]);
                n.m_fieldN[pos].setZonePerso(true);
                n.m_fieldN[pos].setDeplacement(i);
                n.m_fieldN[pos].setPrecedent(file.front().getPosCase());
            }
        }
        if (pos == dest)
        {
            break;
        }
        file.pop();
    }
    precedent = n.m_fieldN[dest].getPrecedent();
    cheminS.push_back(dest);
    while (precedent != n.getPosPlayer())
    {
        for (unsigned int i=0; i < n.m_fieldN.size(); i++)
        {
            if (n.m_fieldN[i].getPosCase() == precedent)
            {
                precedent=n.m_fieldN[i].getPrecedent();
                cheminS.push_back(i);
                //system("pause");
                break;
            }

        }
    }
    cheminS.push_back(n.getPosPlayer());
    cheminSI.resize(cheminS.size());
    for (int i=cheminS.size()-1; i >= 0; i--)
    {
        cheminSI[k] = cheminS[i];
        k++;
    }
    ///Transformer Chemin de Case en Chemin Deplacements

    for (unsigned int i=0; i < cheminSI.size()-1; i++)
    {
        posTOP = Coord::getDirPos(cheminSI[i], TOP);
        posBOTTOM = Coord::getDirPos(cheminSI[i], BOTTOM);
        posLEFT = Coord::getDirPos(cheminSI[i], LEFT);
        posRIGHT = Coord::getDirPos(cheminSI[i], RIGHT);

        if (posTOP == cheminSI[i+1])
            cheminDep.push_back(TOP);
        else if (posBOTTOM == cheminSI[i+1])
            cheminDep.push_back(BOTTOM);
        else if (posLEFT == cheminSI[i+1])
            cheminDep.push_back(LEFT);
        else if (posRIGHT == cheminSI[i+1])
            cheminDep.push_back(RIGHT);
    }
//    for (int i=0; i < cheminDep.size(); i++)
//    {
//       std::cout << cheminDep[i];
//    }
    return cheminDep;
}

void Maze::zoneConnexeVide()
{
    std::queue <Case> file;
    for (unsigned short int i=0; i<m_cases.size(); i++)
    {
        m_cases[i].setisIn(false);
    }

    for (unsigned short i=0; i < m_cases.size(); i++)
    {
        if(m_cases[i].getPosCase() == m_pos_player)
        {
            file.push(m_cases[i]);
            m_cases[i].setisIn(true);
        }
    }
    while (!file.empty())
    {
        for (int i=0; i<4; i++)
        {
            int pos = Coord::getDirPos(file.front().getPosCase(), i);
            if ((m_cases[pos].getType() != SPRITE_WALL) && (!m_cases[pos].getisIn()))
            {
                file.push(m_cases[pos]);
                m_cases[pos].setisIn(true);
            }
        }
        file.pop();
    }
    for (unsigned short i=0; i < m_field.size(); i++)
    {
        if ((!m_cases[i].getisIn()) && (m_cases[i].getType() == SPRITE_GROUND))
        {
            m_field[i]=SPRITE_OUT;
            m_cases[i].setType(SPRITE_OUT);
        }

    }


}


bool Maze::updatePlayer(char dir)
{
    unsigned int pos = Coord::getDirPos(this->getPosPlayer(), dir);
    unsigned int pos2 = Coord::getDirPos(pos, dir);
    bool mvt=true;
    if (dir < 0 || dir > MAX_DIR)
    {
        std::cerr << "Maze::updatePlayer => Direction not correct... " << +dir << std::endl;
        //return false;
    }

    if (this->isSquareWalkable(pos))
    {
        this->setPlayerPos(pos);
        this->setPlayerDir(dir);
    }
    else if (this->isSquareBox(pos))
    {
        //if (this->_canPushBox(Coord::getDirPos(this->getPosPlayer(), dir), dir ))
        if (this->isSquareWalkable(pos2))
        {
            if(this->isSquareGoal(pos2))
            {
                this->m_field[pos2] = SPRITE_BOX_PLACED;
            }
            else if (m_field[pos2] == SPRITE_DEADSQUARE)
            {
                this->m_field[pos2] = SPRITE_BOX;
                this->m_field[pos] = SPRITE_GROUND;
                std::cout << " Jeu Bloque - Reinitialisation du niveau" << std::endl;
                return false;
            }
            else
            {
                this->m_field[pos2] = SPRITE_BOX;

            }
            for(unsigned int j=0; j < this->m_pos_boxes.size(); j++)
            {
                if (this->m_pos_boxes[j] == pos)
                {
                    this->m_pos_boxes[j] = pos2;
                }
                if (pos == m_pos_goals[j])
                {
                    this->m_field[pos] = SPRITE_GOAL;
                }
                else if (this->m_field[pos] != SPRITE_GOAL)
                {
                    this->m_field[pos] = SPRITE_GROUND;
                }
            }
            this->setPlayerPos(pos);
        }
    }
    this->setPlayerDir(dir);
    return mvt;
}

void Maze::initDeadLock()
{
    unsigned short pos, posTOP, posRIGHT, posBOTTOM, posLEFT, posvar;
    bool issue=false;
    //Trouver la ligne du mur du bas

    for (unsigned short i=Coord::m_nb_col; i < m_field.size()-Coord::m_nb_col; i++)
    {
        pos = m_cases[i].getPosCase();
        posTOP=Coord::getDirPos(pos, TOP);
        posRIGHT=Coord::getDirPos(pos, RIGHT);
        posBOTTOM=Coord::getDirPos(pos, BOTTOM);
        posLEFT=Coord::getDirPos(pos, LEFT);

        if ((m_field[pos] == SPRITE_GROUND) && (m_cases[pos].getisIn()))
        {
            if (m_field[posTOP] == SPRITE_WALL)
            {
                posvar = pos;
                issue = false;
                while (m_field[posvar] != SPRITE_WALL)
                {

                    if((m_field[Coord::getDirPos(posvar, TOP)] != SPRITE_WALL) || (m_field[Coord::getDirPos(posvar, RIGHT)] == SPRITE_BOX_PLACED) || (m_field[Coord::getDirPos(posvar, RIGHT)] == SPRITE_GOAL))
                    {
                        issue = true;
                    }
                    posvar ++;
                }
                posvar = pos;
                while (m_field[posvar] != SPRITE_WALL)
                {

                    if((m_field[Coord::getDirPos(posvar, TOP)] != SPRITE_WALL) || (m_field[Coord::getDirPos(posvar, LEFT)] == SPRITE_BOX_PLACED) || (m_field[Coord::getDirPos(posvar, LEFT)] == SPRITE_GOAL))
                    {
                        issue = true;
                    }
                    posvar --;
                }
                if (!issue)
                {
                    std::cout << "pos: " << pos << "Deadlock1" << std::endl;

                    m_field[pos] = SPRITE_DEADSQUARE;
                    m_cases[pos].setType(SPRITE_DEADSQUARE);
                }
            }
            if (m_field[posBOTTOM] == SPRITE_WALL)
            {
                posvar = pos;
                issue = false;
                while (m_field[posvar] != SPRITE_WALL)
                {
                    if((m_field[Coord::getDirPos(posvar, BOTTOM)] != SPRITE_WALL) || (m_field[Coord::getDirPos(posvar, RIGHT)] == SPRITE_BOX_PLACED) || (m_field[Coord::getDirPos(posvar, RIGHT)] == SPRITE_GOAL))
                    {
                        issue = true;
                    }
                    posvar ++;
                }
                posvar = pos;
                while (m_field[posvar] != SPRITE_WALL)
                {
                    if((m_field[Coord::getDirPos(posvar, BOTTOM)] != SPRITE_WALL) || (m_field[Coord::getDirPos(posvar, LEFT)] == SPRITE_BOX_PLACED) || (m_field[Coord::getDirPos(posvar, LEFT)] == SPRITE_GOAL))
                    {
                        issue = true;
                    }
                    posvar --;
                }
                if (!issue)
                {
                    m_field[pos] = SPRITE_DEADSQUARE;
                    m_cases[pos].setType(SPRITE_DEADSQUARE);
                }
            }
            if (m_field[posLEFT] == SPRITE_WALL)
            {
                posvar = pos;
                issue = false;
                while (m_field[posvar] != SPRITE_WALL)
                {
                    if((m_field[Coord::getDirPos(posvar, LEFT)] != SPRITE_WALL) || (m_field[Coord::getDirPos(posvar, TOP)] == SPRITE_BOX_PLACED) || (m_field[Coord::getDirPos(posvar, TOP)] == SPRITE_GOAL))
                    {
                        issue = true; //++
                    }
                    posvar = Coord::getDirPos(posvar, TOP);
                }
                posvar = pos;
                while (m_field[posvar] != SPRITE_WALL)
                {

                    if((m_field[Coord::getDirPos(posvar, LEFT)] != SPRITE_WALL) || (m_field[Coord::getDirPos(posvar, BOTTOM)] == SPRITE_BOX_PLACED) || (m_field[Coord::getDirPos(posvar, BOTTOM)] == SPRITE_GOAL))
                    {
                        issue = true;
                    }
                    posvar = Coord::getDirPos(posvar, BOTTOM);
                }
                if (!issue)
                {
                    m_field[pos] = SPRITE_DEADSQUARE;
                    m_cases[pos].setType(SPRITE_DEADSQUARE);
                }
            }
            if (m_field[posRIGHT] == SPRITE_WALL)
            {
                posvar = pos;
                issue = false;
                while (m_field[posvar] != SPRITE_WALL)
                {
                    if((m_field[Coord::getDirPos(posvar, RIGHT)] != SPRITE_WALL) || (m_field[Coord::getDirPos(posvar, TOP)] == SPRITE_BOX_PLACED) || (m_field[Coord::getDirPos(posvar, TOP)] == SPRITE_GOAL))
                    {
                        issue = true;
                    }
                    posvar = Coord::getDirPos(posvar, TOP);
                }
                posvar = pos;
                while (m_field[posvar] != SPRITE_WALL)
                {
                    if((m_field[Coord::getDirPos(posvar, RIGHT)] != SPRITE_WALL) || (m_field[Coord::getDirPos(posvar, BOTTOM)] == SPRITE_BOX_PLACED) || (m_field[Coord::getDirPos(posvar, BOTTOM)] == SPRITE_GOAL))
                    {
                        issue = true;
                    }
                    posvar = Coord::getDirPos(posvar, BOTTOM);
                }
                if (!issue)
                {
                    std::cout << "pos: " << pos << "Deadlock4" << std::endl;

                    m_field[pos] = SPRITE_DEADSQUARE;
                    m_cases[pos].setType(SPRITE_DEADSQUARE);
                }
            }

            switch (m_cases[i].getOrdre())
            {
                case 0 :    m_field[pos] = SPRITE_DEADSQUARE;
                            std::cout << "pos: " << pos << "Deadlock" << std::endl;


                            break;

                case 1 :    if ((m_field[posTOP] == SPRITE_WALL) && (m_field[posLEFT] == SPRITE_WALL))
                            {
                                m_field[pos] = SPRITE_DEADSQUARE;
                                m_cases[pos].setType(SPRITE_DEADSQUARE);
                            }

                            if ((m_field[posTOP] == SPRITE_WALL) && (m_field[posRIGHT] == SPRITE_WALL))
                            {
                                m_field[pos] = SPRITE_DEADSQUARE;
                                m_cases[pos].setType(SPRITE_DEADSQUARE);
                            }
                            if ((m_field[posBOTTOM] == SPRITE_WALL) && (m_field[posLEFT] == SPRITE_WALL))
                            {
                                m_field[pos] = SPRITE_DEADSQUARE;
                                m_cases[pos].setType(SPRITE_DEADSQUARE);
                            }
                            if ((m_field[posBOTTOM] == SPRITE_WALL) && (m_field[posRIGHT] == SPRITE_WALL))
                            {
                                m_field[pos] = SPRITE_DEADSQUARE;
                                m_cases[pos].setType(SPRITE_DEADSQUARE);
                            }
                            break;

                case 2 :    if ((m_field[posTOP] == SPRITE_WALL) && (m_field[posLEFT] == SPRITE_WALL))
                            {
                                m_field[pos] = SPRITE_DEADSQUARE;
                                m_cases[pos].setType(SPRITE_DEADSQUARE);
                            }
                            if ((m_field[posTOP] == SPRITE_WALL) && (m_field[posRIGHT] == SPRITE_WALL))
                            {
                                m_field[pos] = SPRITE_DEADSQUARE;
                                m_cases[pos].setType(SPRITE_DEADSQUARE);
                            }
                            if ((m_field[posBOTTOM] == SPRITE_WALL) && (m_field[posLEFT] == SPRITE_WALL))
                            {
                                m_field[pos] = SPRITE_DEADSQUARE;
                                m_cases[pos].setType(SPRITE_DEADSQUARE);
                            }
                            if ((m_field[posBOTTOM] == SPRITE_WALL) && (m_field[posRIGHT] == SPRITE_WALL))
                            {
                                m_field[pos] = SPRITE_DEADSQUARE;
                                m_cases[pos].setType(SPRITE_DEADSQUARE);
                            }
                            break;

                case 3 :
                            break;

            }

        }
    }
}




// DO NOT TOUCH !
// Overload function for displaying debug information
// about Maze class
std::ostream& operator << (std::ostream& O, const Maze& m)
{
    unsigned int l, c;
    int i = 0;
    Coord::coord2D(m.m_pos_player, l, c);
    O << "Player position " << m.m_pos_player << " (" << l << "," << c << ")" << std::endl;
    O << "Field Size " << +m.m_lig << " x " << +m.m_col << " = " << m.getSize() << std::endl;
    O << "Field Vector capacity : " << m.m_field.capacity() << std::endl;
    O << "Field array : " << std::endl << std::endl;
    for(unsigned int l=0; l<m.getSize(); l++)
    {
        if (l == m.m_pos_player) Console::getInstance()->setColor(_COLOR_YELLOW);
        else if (m.isSquareWall(l)) Console::getInstance()->setColor(_COLOR_PURPLE);
        else if (m.isSquareBoxPlaced(l) || m.isSquareGoal(l)) Console::getInstance()->setColor(_COLOR_GREEN);
        else if (m.isSquareBox(l)) Console::getInstance()->setColor(_COLOR_BLUE);
        else if (m.m_field[l] == SPRITE_DEADSQUARE) Console::getInstance()->setColor(_COLOR_RED);
        else Console::getInstance()->setColor(_COLOR_WHITE);

        O << std::setw(2) << +m.m_field[l] << " "; // + => print as "int"

        if ((l+1) % m.m_col == 0)
        {
            O << std::endl;
        }
    }
    Console::getInstance()->setColor(_COLOR_DEFAULT);

    O << std::endl;
    O << "Box position : " << std::endl;
    for (unsigned int i=0; i<m.m_pos_boxes.size(); i++)
    {
        Coord::coord2D(m.m_pos_boxes[i], l, c);
        O << "\t" << "Box #" << i << " => " << std::setw(3) << m.m_pos_boxes[i] << std::setw(2) << " (" << l << "," << c << ")" << std::endl;
    }

    O << std::endl;
    O << "Goal position : " << std::endl;
    for (const auto& goal : m.m_pos_goals)
    {
        unsigned int l, c;
        Coord::coord2D(goal, l, c);
        if (m.isSquareBoxPlaced(goal)) Console::getInstance()->setColor(_COLOR_GREEN);
        O << "\t" << "Goal #" << i << " => " << std::setw(3) << goal << std::setw(2) << " (" << l << "," << c << ")" << std::endl;
        if (m.isSquareBoxPlaced(goal)) Console::getInstance()->setColor(_COLOR_DEFAULT);
        i++;
    }

    return O;
}
