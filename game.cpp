#include "game.h"
#include <string>
#include <iostream>
#include <cstdlib>




Game::Game()
{
    
}
Game::~Game()
{
    
}

std::string Game::choixniveau()
{
    char choix, choix2;
    std::string path = "./levels/", difficult, extension=".dat";
    
    do
    {
        system("cls");
        std::cout << "Quel difficulte souhaitez-vous ?" << std::endl << std::endl;
        std::cout << " 1. Facile" << std::endl;
        std::cout << " 2. Moyenne" << std::endl;
        std::cout << " 3. Dificile" << std::endl;
        std::cout << " 4. Test" << std::endl << std::endl;
        std::cout << " 5. Retour" << std::endl;
        
        std::cin >> choix;
        switch (choix)
        {
            case '1':   difficult="easy/easy_";
                std::cout << "Il existe 10 niveaux faciles. Choisir le niveau a charger : ";
                do
                {
                    std::cin >> choix2;
                }while((choix2 <'1') || (choix2 > '9'));
                break;
                
            case '2':   difficult="medium/medium_";
                std::cout << "Il existe 3 niveaux moyens. Choisir le niveau a charger : ";
                do
                {
                    std::cin >> choix2;
                }while((choix2 <'1') || (choix2 > '3'));
                break;
                
            case '3':   difficult="hard/hard_";
                std::cout << "Il existe 3 niveaux dificiles. Choisir le niveau a charger : ";
                do
                {
                    std::cin >> choix2;
                }while((choix2 <'1') || (choix2 > '3'));
                break;
                
            case '4':   difficult="test/test_";
                std::cout << "Il existe 4 niveaux test. Choisir le niveau a charger : ";
                do
                {
                    std::cin >> choix2;
                }while((choix2 <'1') || (choix2 > '4'));
                extension=".txt";
                break;
            case '5':   path = "retour";
                return path;
                
            default:    break;
        }
    }while ((choix != '1') && (choix != '2') && (choix != '3') && (choix != '4')&& (choix != '5'));
    
    
    path = path + difficult + choix2 + extension;
    std::cout << "path : " << path;
    return path;
}

int Game::jouer(std::string path,Graphic g)
{
    // Do not touch !
    Console::getInstance()->setColor(_COLOR_DEFAULT);
    
    // Load level from a file
    Maze m(path.c_str());
    //    Maze m("./levels/easy/easy_10.txt");
    if (!m.init()) return -1;
    
    std::cout << "start alloc" << std::endl;
    m.alloc_case();
    m.zoneConnexeVide();
    m.initDeadLock();
    
    std::cout << m << std::endl;
    m.infosCases();
    
    // While playing...
    while (!g.keyGet(KEY_ESC) && !g.keyGet(KEY_A) && !m.isCompleted())
    {
        // Check if user has pressed a key
        if (g.keyPressed())
        {
            bool win = true;
            switch (g.keyRead())
            {
                case ARROW_UP:      win = m.updatePlayer(TOP);
                    break;
                case ARROW_BOTTOM:  win = m.updatePlayer(BOTTOM);
                    break;
                case ARROW_RIGHT:   win = m.updatePlayer(RIGHT);
                    break;
                case ARROW_LEFT:    win = m.updatePlayer(LEFT);
                    break;
            }
            if (!win)
            {
                m.del();
                if (!m.init()) return -1;
                m.alloc_case();
                m.zoneConnexeVide();
                m.initDeadLock();
            }
        }
        if(m.isCompleted())
        {
            std::cout << "Victoire" << std::endl;
        }
        // Display on screen
        g.clear();
        m.draw(g);
        g.display(Coord::m_nb_col);
    }
    
    Console::deleteInstance();
    std::cout << m << std::endl;
    
    return 0;
}

void Game::menu(Graphic g)
{
    bool quit=false;
    std::string path;
    int joue;
    
    do
    {
        
        if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=252)&&(mouse_y<=304))
        {
            //Eviter l'enchainement des menus si le clic est maintenu
            if(mouse_b&1){do{}while (mouse_b&1);}
            path=menuChoix(g);
            joue=jouer(path,g);
            
        }
        else if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=340)&&(mouse_y<=391))
        {
            //Eviter l'enchainement des menus si le clic est maintenu
            if(mouse_b&1){do{}while (mouse_b&1);}
            this->menu2(g);
        }
        else if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=438)&&(mouse_y<=488))
        {
            //Eviter l'enchainement des menus si le clic est maintenu
            if(mouse_b&1){do{}while (mouse_b&1);}
            quit=true;
        }
        g.clear();
        draw_sprite(g.pBuffer, g.pSprites[10], 0,0);
        g.display(0, false);
        
    }while(!quit);
    
    
}

void Game::menuChargement(Graphic g)
{
    g.clear();
    draw_sprite(g.pBuffer, g.pSprites[13], 0,0);
    g.display(0, false);
}

void Game::menu2(Graphic g)
{
    bool quit=false;
    int win;
    
    std::string path;
    draw_sprite(g.pBuffer, g.pSprites[11], 0,0);
    g.display(0, false);
    
    do
    {
        g.display(0, false);
        if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=252)&&(mouse_y<=304))
        {
            //Eviter l'enchainement des menus si le clic est maintenu
            if(mouse_b&1){do{}while (mouse_b&1);}
            path = menuChoix(g);
            win = bruteforce(path,g);
            quit = true;
        }
        else if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=340)&&(mouse_y<=391))
        {
            //Eviter l'enchainement des menus si le clic est maintenu
            if(mouse_b&1){do{}while (mouse_b&1);}
            path = menuChoix(g);
            win = bfs(path,g);
            
            quit = true;
        }
        else if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=438)&&(mouse_y<=488))
        {
            //Eviter l'enchainement des menus si le clic est maintenu
            if(mouse_b&1){do{}while (mouse_b&1);}
            quit=true;
        }
        
    }while(!quit);
}

std::string Game::menuChoix(Graphic g)
{
    bool quit = false;
    std::string path;
    draw_sprite(g.pBuffer, g.pSprites[12], 0,0);
    g.display(0, false);
    
    do
    {
        ///FACILE
        if((mouse_b&1)&&(mouse_x>=46)&&(mouse_x<=97)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_1.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=122)&&(mouse_x<=172)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_2.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=200)&&(mouse_x<=250)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_3.txt";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=278)&&(mouse_x<=328)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_4.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=305)&&(mouse_x<=406)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_5.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=423)&&(mouse_x<=473)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_6.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=495)&&(mouse_x<=546)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_7.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=568)&&(mouse_x<=617)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_8.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=643)&&(mouse_x<=695)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_9.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=720)&&(mouse_x<=770)&&(mouse_y>=93)&&(mouse_y<=133))
        {
            path="./levels/easy/easy_10.dat";
            quit=true;
        }
        ///MOYEN
        else if((mouse_b&1)&&(mouse_x>=207)&&(mouse_x<=256)&&(mouse_y>=233)&&(mouse_y<=274))
        {
            path="./levels/medium/medium_1.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=373)&&(mouse_x<=425)&&(mouse_y>=233)&&(mouse_y<=274))
        {
            path="./levels/medium/medium_2.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=538)&&(mouse_x<=588)&&(mouse_y>=233)&&(mouse_y<=274))
        {
            path="./levels/medium/medium_3.dat";
            quit=true;
        }
        ///DIFFICILE
        else if((mouse_b&1)&&(mouse_x>=205)&&(mouse_x<=256)&&(mouse_y>=373)&&(mouse_y<=414))
        {
            path="./levels/hard/hard_1.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=379)&&(mouse_x<=425)&&(mouse_y>=373)&&(mouse_y<=414))
        {
            path="./levels/hard/hard_2.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=535)&&(mouse_x<=587)&&(mouse_y>=373)&&(mouse_y<=414))
        {
            path="./levels/hard/hard_3.dat";
            quit=true;
        }
        ///TEST
        else if((mouse_b&1)&&(mouse_x>=208)&&(mouse_x<=258)&&(mouse_y>=530)&&(mouse_y<=571))
        {
            path="./levels/test/test_1.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=321)&&(mouse_x<=373)&&(mouse_y>=530)&&(mouse_y<=571))
        {
            path="./levels/test/test_2.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=442)&&(mouse_x<=492)&&(mouse_y>=530)&&(mouse_y<=571))
        {
            path="./levels/test/test_3.dat";
            quit=true;
        }
        else if((mouse_b&1)&&(mouse_x>=538)&&(mouse_x<=590)&&(mouse_y>=530)&&(mouse_y<=571))
        {
            path="./levels/test/test_4.txt";
            quit=true;
        }
        
    }while(!quit);
    return path;
}


int Game::bfs(std::string path, Graphic g)
{
    menuChargement(g);
    std::queue<Noeud> file;
    std::vector<int> cheminN;
    std::vector<Noeud> cheminNoeudI, cheminNoeud;
    // Do not touch !
    Console::getInstance()->setColor(_COLOR_DEFAULT);
    // Load level from a file
    Maze m(path.c_str());
    //    Maze m("./levels/easy/easy_10.txt");
    if (!m.init()) return -1;
    
    std::cout << "start alloc" << std::endl;
    m.alloc_case();
    m.zoneConnexeVide();
    m.initDeadLock();
    m.allocNoeuds();
    std::cout << m << std::endl;
    file.push(m.m_etats[0]);
    
    while (!file.empty())
    {
        file = m.newNoeud(file);
        if (file.front().isCompleted())
        {
            std::cout << "VICTOIRE " <<file.front().getNumber() << std::endl;
            int preced = file.front().getPreced();
            cheminN.push_back(preced);
            cheminNoeudI.push_back(file.front());
            while (preced != 0)
            {
                for (unsigned int i=0; i < m.m_etats.size(); i++)
                {
                    if (m.m_etats[i].getNumber() == preced)
                    {
                        preced=m.m_etats[i].getPreced();
                        cheminN.push_back(preced);
                        cheminNoeudI.push_back(m.m_etats[i]);
                        break;
                    }
                }
            }
            cheminNoeudI.push_back(m.m_etats[0]);
            for (int i=cheminNoeudI.size()-1; i >= 0 ; i--)
            {
                cheminNoeud.push_back(cheminNoeudI[i]);
            }
            std::cout << std::endl;
            
            m.calculCheminSolution(cheminNoeud);
            break;
        }
        
        file.pop();
    }
    m.affichageCheminSolution(g);
    Sleep(500);
    return 0;
}


int Game::bruteforce(std::string path, Graphic g)
{
    
    std::vector<int> cheminc;
    bool win;
    int n=0;
    
    Console::getInstance()->setColor(_COLOR_DEFAULT);
    // Load level from a file
    Maze m(path.c_str());
    if (!m.init()) return -1;
    
    m.alloc_case();
    m.zoneConnexeVide();
    
    //m.initDeadLock();
    std::cout << m << std::endl;
    
    while (!m.isCompleted())
    {
        std::vector<int> chemin;
        //Augmentation de l'ordre
        n++;
        
        chemin.resize(n);
        for (int i=0; i<n; i++)
        {
            chemin[i]=0;
        }
        
        for (int i=1; i < pow(4,n); i++)
        {
            chemin[n-1]++;
            for (int j=n; j>0; j--)
            {
                if (chemin[j]==4)
                {
                    chemin[j]=0;
                    chemin[j-1]++;
                }
            }
            for (int j=0; j<n; j++)
            {
                win = m.updatePlayer(chemin[j]);
                g.clear();
                m.draw(g);
                g.display(Coord::m_nb_col);
                //usleep(5000);
            }
            if(m.isCompleted())
            {
                cheminc.resize(n-1);
                for (int x=0; x<n; x++)
                {
                    cheminc[x]=chemin[x];
                }
                break;
            }
            m.del();
            if (!m.init()) return -1;
            //m.initDeadLock();
            ///Display on screen
            g.clear();
            m.draw(g);
            g.display(Coord::m_nb_col);
        }
        
    }
    for (unsigned  int x=0; x<=cheminc.size(); x++)
    {
        std::cout << cheminc[x];
        m.m_cheminSolution.push_back(cheminc[x]);
        std::cout << m.m_cheminSolution[x];
    }
    std::cout << std::endl;
    Sleep(1000);
    m.del();
    if (!m.init()) return -1;
    m.affichageCheminSolution(g);
    Console::deleteInstance();
    return 0;
    
}
