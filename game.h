#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "maze.h"
#include "utils/console.h"
#include "utils/coord.h"
#include "graphic.h"
#include <string>
#include <math.h>
#include <unistd.h>
#include <winalleg.h>


class Game

{
    
public:
    
    Game();
    ~Game();
    std::string choixniveau();
    void menu(Graphic g);
    void menuChargement(Graphic g);
    void menu2(Graphic g);
    std::string menuChoix(Graphic g);
    int jouer(std::string path,Graphic g);
    int bruteforce(std::string path, Graphic g);
    int bfs(std::string path, Graphic g);
    int dfs(std::string path, Graphic g);
};



#endif // GAME_H_INCLUDED
