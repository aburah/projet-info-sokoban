/************************************************************
 Sokoban project - Graphic file
 Copyright Florent DIEDLER
 Date : 27/02/2016
 
 Please do not remove this header, if you use this file !
 ************************************************************/

#include "graphic.h"
#include "game.h"
#include <winalleg.h>

Graphic::Graphic(int _resX, int _resY, int _depth)
: pBuffer(NULL), m_resX(_resX), m_resY(_resY), m_depth(_depth)
{
    for (int i=0; i<MAX_SPRITE; i++)
        pSprites[i] = NULL;
    for (int i=0; i<MAX_SPRITE_PLAYER; i++)
        pSpritesPlayer[i] = NULL;
}

Graphic::~Graphic()
{
    
}

bool Graphic::_imageLoadSprites()
{
    this->pBuffer = imageCreate(m_resX, m_resY, COLOR_WHITE);
    if (!this->pBuffer)
    {
        std::cerr << "Cannot allocate buffer..." << std::endl;
        return 0;
    }
    
    // chargement des bitmaps du jeu
    this->pSprites[0] = imageCreate(TX, TY, COLOR_WHITE);
    this->pSprites[1] = imageLoad("images/mur.bmp");
    this->pSprites[2] = imageLoad("images/caisse.bmp");
    this->pSprites[3] = imageLoad("images/caisse_ok.bmp");
    this->pSprites[4] = imageLoad("images/objectif.bmp");
    this->pSprites[5] = imageCreate(TX, TY, COLOR_ROSE);
    this->pSprites[6] = imageCreate(TX, TY, COLOR_WHITE);
    this->pSprites[7] = imageCreate(TX, TY, COLOR_YELLOW);
    this->pSprites[8] = imageCreate(TX, TY, COLOR_GREEN);
    this->pSprites[9] = imageCreate(TX, TY, COLOR_RED);
    
    this->pSprites[10] = imageLoad("images/fond.bmp");
    this->pSprites[11] = imageLoad("images/fond2.bmp");
    this->pSprites[12] = imageLoad("images/choixniveau.bmp");
    this->pSprites[13] = imageLoad("images/calcul.bmp");
    
    
    this->pSpritesPlayer[0] = imageLoad("images/mario_haut.bmp");
    this->pSpritesPlayer[1] = imageLoad("images/mario_bas.bmp");
    this->pSpritesPlayer[2] = imageLoad("images/mario_gauche.bmp");
    this->pSpritesPlayer[3] = imageLoad("images/mario_droite.bmp");
    
    for (int i=0; i<MAX_SPRITE; i++)
    {
        if (this->pSprites[i] == NULL)
        {
            return false;
        }
    }
    for (int i=0; i<MAX_SPRITE_PLAYER; i++)
    {
        if (this->pSpritesPlayer[i] == NULL)
        {
            return false;
        }
    }
    
    return true;
}

bool Graphic::init()
{
    set_color_depth(m_depth); // profondeur des couleurs 8, 16, 24 ou 32
    set_uformat(U_ASCII); // pour g�rer les accents
    
    allegro_init();
    
    if (install_mouse() == -1)
    {
        std::cerr << ("Failed to install mouse") << std::endl;
        return false;
    }
    if (install_keyboard() != 0)
    {
        std::cerr << ("Failed to install keyboard") << std::endl;
        return false;
    }
    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, m_resX, m_resY, 0, 0) != 0)
    {
        std::cerr << ("Failed to enter in graphic mode") << std::endl;
        return false;
    }
    
    set_keyboard_rate(500, 100);
    
    show_mouse(screen);
    
    return this->_imageLoadSprites();
}

void Graphic::deinit()
{
    for (int i=0; i<MAX_SPRITE; i++)
    {
        imageDestroy(this->pSprites[i]);
    }
    for (int i=0; i<MAX_SPRITE_PLAYER; i++)
    {
        imageDestroy(this->pSpritesPlayer[i]);
    }
    
    imageDestroy(pBuffer);
    remove_mouse();
    remove_keyboard();
    allegro_exit();
}

pSurface Graphic::imageLoad(const std::string& fileName)
{
    return load_bitmap(fileName.c_str(), NULL);
}

pSurface Graphic::imageCreate(int width, int height, int color)
{
    pSurface s = NULL;
    s = create_bitmap(width, height);
    if (s != NULL)
        imageClearToColor(s, color);
    
    return s;
}

void Graphic::imageDestroy(pSurface s)
{
    destroy_bitmap(s);
}

void Graphic::imageClearToColor(const pSurface s, int color)
{
    clear_to_color(s, color);
}

void Graphic::clear()
{
    imageClearToColor(pBuffer, COLOR_WHITE);
}

void Graphic::drawCircle(int x, int y, int radius, int color, int fill, bool coord) const
{
    if (coord)
    {
        x = x*TX + TX/2;
        y = y*TY + TY/2;
    }
    
    if (fill)
        circlefill(pBuffer, x, y, radius, color);
    else circle(pBuffer, x, y, radius, color);
}

void Graphic::drawRect(int x1, int y1, int x2, int y2, int color, int fill, bool coord) const
{
    if (coord)
    {
        x1 *= TX;
        y1 *= TY;
        x2 *= TX;
        y2 *= TY;
    }
    
    if (fill==1)
        rectfill(pBuffer, x1, y1, x2, y2, color);
    else if (fill > 1)
    {
        rect(pBuffer, x1-1, y1-1, x2+1, y2+1, color);
        rect(pBuffer, x1, y1, x2, y2, color);
        rect(pBuffer, x1+1, y1+1, x2-1, y2-1, color);
    }
    else rect(pBuffer, x1, y1, x2, y2, color);
}

void Graphic::drawT(const pSurface src, int x, int y, bool coord) const
{
    if (coord)
    {
        x *= TX;
        y *= TY;
    }
    draw_sprite(pBuffer, src, x, y);
}

void Graphic::draw(const pSurface src, int source_x, int source_y, int dest_x, int dest_y, int width, int height, bool coord) const
{
    if (coord)
    {
        source_x *= TX;
        source_y *= TY;
        dest_x *= TX;
        dest_y *= TY;
    }
    blit(src, pBuffer, source_x, source_y, dest_x, dest_y, width, height);
}

void Graphic::display(int nbCol, bool withGrid)
{
    if (withGrid) this->_grid(COLOR_BLACK);
    //draw_sprite(screen, pBuffer, 0, 0);
    
    if (nbCol > 0)
    {
        int col = mouse_x / TX;
        int lig = mouse_y / TY;
        textprintf_ex(pBuffer, font, mouse_x, mouse_y, COLOR_BLUE, -1,
                      "Pos = %d", (lig * nbCol + col));
    }
    
    blit(pBuffer, screen, 0, 0, 0, 0, pBuffer->w, pBuffer->h);
    
    rest(20); // tempo
}

/*
 
 void Graphic::menu()
 {
 bool quit=false;
 std::string path;
 Game jeu;
 do
 {
 
 if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=252)&&(mouse_y<=304))
 {
 //Eviter l'enchainement des menus si le clic est maintenu
 if(mouse_b&1){do{}while (mouse_b&1);}
 path = this->menuChoix();
 jeu.jouer(path);
 
 }
 else if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=340)&&(mouse_y<=391))
 {
 //Eviter l'enchainement des menus si le clic est maintenu
 if(mouse_b&1){do{}while (mouse_b&1);}
 this->menu2();
 }
 else if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=438)&&(mouse_y<=488))
 {
 //Eviter l'enchainement des menus si le clic est maintenu
 if(mouse_b&1){do{}while (mouse_b&1);}
 quit=true;
 }
 this->clear();
 draw_sprite(pBuffer, this->pSprites[10], 0,0);
 this->display(0, false);
 
 }while(!quit);
 
 
 }
 
 void Graphic::menuChargement()
 {
 this->clear();
 draw_sprite(pBuffer, this->pSprites[13], 0,0);
 this->display(0, false);
 }
 
 void Graphic::menu2()
 {
 bool quit=false;
 int win;
 
 std::string path;
 draw_sprite(pBuffer, this->pSprites[11], 0,0);
 this->display(0, false);
 
 do
 {
 this->display(0, false);
 if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=252)&&(mouse_y<=304))
 {
 //Eviter l'enchainement des menus si le clic est maintenu
 if(mouse_b&1){do{}while (mouse_b&1);}
 path = this->menuChoix();
 win = bruteforce(path);
 quit = true;
 }
 else if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=340)&&(mouse_y<=391))
 {
 //Eviter l'enchainement des menus si le clic est maintenu
 if(mouse_b&1){do{}while (mouse_b&1);}
 path = this->menuChoix();
 win = bfs(path);
 
 quit = true;
 }
 else if((mouse_b&1)&&(mouse_x>=286)&&(mouse_x<=514)&&(mouse_y>=438)&&(mouse_y<=488))
 {
 //Eviter l'enchainement des menus si le clic est maintenu
 if(mouse_b&1){do{}while (mouse_b&1);}
 quit=true;
 }
 
 }while(!quit);
 }
 
 std::string Graphic::menuChoix()
 {
 bool quit = false;
 std::string path;
 draw_sprite(pBuffer, this->pSprites[12], 0,0);
 this->display(0, false);
 
 do
 {
 ///FACILE
 if((mouse_b&1)&&(mouse_x>=46)&&(mouse_x<=97)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_1.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=122)&&(mouse_x<=172)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_2.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=200)&&(mouse_x<=250)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_3.txt";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=278)&&(mouse_x<=328)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_4.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=305)&&(mouse_x<=406)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_5.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=423)&&(mouse_x<=473)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_6.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=495)&&(mouse_x<=546)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_7.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=568)&&(mouse_x<=617)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_8.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=643)&&(mouse_x<=695)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_9.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=720)&&(mouse_x<=770)&&(mouse_y>=93)&&(mouse_y<=133))
 {
 path="./levels/easy/easy_10.dat";
 quit=true;
 }
 ///MOYEN
 else if((mouse_b&1)&&(mouse_x>=207)&&(mouse_x<=256)&&(mouse_y>=233)&&(mouse_y<=274))
 {
 path="./levels/medium/medium_1.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=373)&&(mouse_x<=425)&&(mouse_y>=233)&&(mouse_y<=274))
 {
 path="./levels/medium/medium_2.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=538)&&(mouse_x<=588)&&(mouse_y>=233)&&(mouse_y<=274))
 {
 path="./levels/medium/medium_3.dat";
 quit=true;
 }
 ///DIFFICILE
 else if((mouse_b&1)&&(mouse_x>=205)&&(mouse_x<=256)&&(mouse_y>=373)&&(mouse_y<=414))
 {
 path="./levels/hard/hard_1.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=379)&&(mouse_x<=425)&&(mouse_y>=373)&&(mouse_y<=414))
 {
 path="./levels/hard/hard_2.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=535)&&(mouse_x<=587)&&(mouse_y>=373)&&(mouse_y<=414))
 {
 path="./levels/hard/hard_3.dat";
 quit=true;
 }
 ///TEST
 else if((mouse_b&1)&&(mouse_x>=208)&&(mouse_x<=258)&&(mouse_y>=530)&&(mouse_y<=571))
 {
 path="./levels/test/test_1.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=321)&&(mouse_x<=373)&&(mouse_y>=530)&&(mouse_y<=571))
 {
 path="./levels/test/test_2.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=442)&&(mouse_x<=492)&&(mouse_y>=530)&&(mouse_y<=571))
 {
 path="./levels/test/test_3.dat";
 quit=true;
 }
 else if((mouse_b&1)&&(mouse_x>=538)&&(mouse_x<=590)&&(mouse_y>=530)&&(mouse_y<=571))
 {
 path="./levels/test/test_4.txt";
 quit=true;
 }
 
 }while(!quit);
 return path;
 }
 */

void Graphic::cursor(int l, int c, int color)
{
    drawRect(c, l, (c+1), (l+1), color, 0);
}

void Graphic::_grid(int color)
{
    int nbL = 25; //pBuffer->w/TX;
    int nbC = pBuffer->h/TY;
    for(int i=0; i<nbL; i++)
        for(int j=0; j<nbC; j++)
            drawRect(i, j, (i+1), (j+1), color, 0);
}


