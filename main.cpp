/************************************************************
Sokoban project - Main file
Copyright Florent DIEDLER
Date : 27/02/2016

Please do not remove this header, if you use this file !
************************************************************/
#include "maze.h"
#include "graphic.h"
#include "utils/console.h"
#include "utils/coord.h"
#include "game.h"

#include <iostream>
#include <iomanip>
#include <math.h>
#include <unistd.h>
#include <winalleg.h>

// Init allegro with 800x600 (resolution)
Graphic g(800,600,32);

/*
int jouer(std::string path)
{
    // Do not touch !
    Console::getInstance()->setColor(_COLOR_DEFAULT);

    // Load level from a file
    Maze m(path.c_str());
//    Maze m("./levels/easy/easy_10.txt");
    if (!m.init()) return -1;

    std::cout << "start alloc" << std::endl;
    m.alloc_case();
    m.zoneConnexeVide();
    m.initDeadLock();

    std::cout << m << std::endl;
    m.infosCases();

    // While playing...
    while (!g.keyGet(KEY_ESC) && !g.keyGet(KEY_A) && !m.isCompleted())
    {
        // Check if user has pressed a key
        if (g.keyPressed())
        {
            bool win = true;
            switch (g.keyRead())
            {
                case ARROW_UP:      win = m.updatePlayer(TOP);
                                    break;
        case ARROW_BOTTOM:  win = m.updatePlayer(BOTTOM);
                                    break;
                case ARROW_RIGHT:   win = m.updatePlayer(RIGHT);
                                    break;
                case ARROW_LEFT:    win = m.updatePlayer(LEFT);
                                    break;
            }
            if (!win)
            {
                m.del();
                if (!m.init()) return -1;
                m.alloc_case();
                m.zoneConnexeVide();
                m.initDeadLock();
            }
        }
        if(m.isCompleted())
        {
            std::cout << "Victoire" << std::endl;
        }
        // Display on screen
        g.clear();
        m.draw(g);
        g.display(Coord::m_nb_col);
    }

    Console::deleteInstance();
    std::cout << m << std::endl;

    return 0;
}



int bruteforce(std::string path)
{

    std::vector<int> cheminc;
    bool win;
    int n=0;

    Console::getInstance()->setColor(_COLOR_DEFAULT);
    // Load level from a file
    Maze m(path.c_str());
    if (!m.init()) return -1;

    m.alloc_case();
    m.zoneConnexeVide();

    //m.initDeadLock();
    std::cout << m << std::endl;

    while (!m.isCompleted())
    {
        std::vector<int> chemin;
        //Augmentation de l'ordre
        n++;

        chemin.resize(n);
        for (int i=0; i<n; i++)
        {
            chemin[i]=0;
        }

        for (int i=1; i < pow(4,n); i++)
        {
            chemin[n-1]++;
            for (int j=n; j>0; j--)
            {
                if (chemin[j]==4)
                {
                    chemin[j]=0;
                    chemin[j-1]++;
                }
            }
            for (int j=0; j<n; j++)
            {
                win = m.updatePlayer(chemin[j]);
                g.clear();
                m.draw(g);
                g.display(Coord::m_nb_col);
                //usleep(5000);
            }
            if(m.isCompleted())
            {
                cheminc.resize(n-1);
                for (int x=0; x<n; x++)
                {
                     cheminc[x]=chemin[x];
                }
                break;
            }
            m.del();
            if (!m.init()) return -1;
            //m.initDeadLock();
            ///Display on screen
            g.clear();
            m.draw(g);
            g.display(Coord::m_nb_col);
        }

    }
    for (unsigned  int x=0; x<=cheminc.size(); x++)
    {
         std::cout << cheminc[x];
         m.m_cheminSolution.push_back(cheminc[x]);
         std::cout << m.m_cheminSolution[x];
    }
    std::cout << std::endl;
    Sleep(1000);
    m.del();
    if (!m.init()) return -1;
    m.affichageCheminSolution(g);
    Console::deleteInstance();
    return 0;

}

int bfs(std::string path)
{
    g.menuChargement();
    std::queue<Noeud> file;
    std::vector<int> cheminN;
    std::vector<Noeud> cheminNoeudI, cheminNoeud;
    // Do not touch !
    Console::getInstance()->setColor(_COLOR_DEFAULT);
    // Load level from a file
    Maze m(path.c_str());
//    Maze m("./levels/easy/easy_10.txt");
    if (!m.init()) return -1;

    std::cout << "start alloc" << std::endl;
    m.alloc_case();
    m.zoneConnexeVide();
    m.initDeadLock();
    m.allocNoeuds();
    std::cout << m << std::endl;
    file.push(m.m_etats[0]);

    while (!file.empty())
    {
        file = m.newNoeud(file);
        if (file.front().isCompleted())
        {
            std::cout << "VICTOIRE " <<file.front().getNumber() << std::endl;
            int preced = file.front().getPreced();
            cheminN.push_back(preced);
            cheminNoeudI.push_back(file.front());
            while (preced != 0)
            {
                for (unsigned int i=0; i < m.m_etats.size(); i++)
                {
                    if (m.m_etats[i].getNumber() == preced)
                    {
                        preced=m.m_etats[i].getPreced();
                        cheminN.push_back(preced);
                        cheminNoeudI.push_back(m.m_etats[i]);
                        break;
                    }
                }
            }
            cheminNoeudI.push_back(m.m_etats[0]);
            for (int i=cheminNoeudI.size()-1; i >= 0 ; i--)
            {
                cheminNoeud.push_back(cheminNoeudI[i]);
            }
            std::cout << std::endl;

            m.calculCheminSolution(cheminNoeud);
            break;
        }

        file.pop();
    }
    m.affichageCheminSolution(g);
    return 0;
}


int dfs(std::string path)
{
    std::stack<Noeud> pile;
    std::vector<int> cheminN;
    std::vector<Noeud> cheminNoeudI, cheminNoeud;
    // Do not touch !
    Console::getInstance()->setColor(_COLOR_DEFAULT);
    // Load level from a file
    Maze m(path.c_str());
//    Maze m("./levels/easy/easy_10.txt");
    if (!m.init()) return -1;

    std::cout << "start alloc" << std::endl;
    m.alloc_case();
    m.zoneConnexeVide();
    m.initDeadLock();
    m.allocNoeuds();
    std::cout << m << std::endl;
    pile.push(m.m_etats[0]);

    while (!pile.empty())
    {
        pile = m.newNoeudP(pile);
        if (pile.top().isCompleted())
        {
            std::cout << "VICTOIRE " <<pile.top().getNumber() << std::endl;
            int preced = pile.top().getPreced();
            cheminN.push_back(preced);
            cheminNoeudI.push_back(pile.top());
            while (preced != 0)
            {
                for (unsigned int i=0; i < m.m_etats.size(); i++)
                {
                    if (m.m_etats[i].getNumber() == preced)
                    {
                        preced=m.m_etats[i].getPreced();
                        cheminN.push_back(preced);
                        cheminNoeudI.push_back(m.m_etats[i]);
                        break;
                    }
                }
            }
            cheminNoeudI.push_back(m.m_etats[0]);
            for (int i=cheminNoeudI.size()-1; i >= 0 ; i--)
            {
                cheminNoeud.push_back(cheminNoeudI[i]);
            }
            std::cout << std::endl;

            m.calculCheminSolution(cheminNoeud);
            break;
        }

        pile.pop();
    }
    m.affichageCheminSolution(g);
    system("pause");
    return 0;
}

*/
// Display maze on screen with Allegro
void Maze::draw(const Graphic& g) const
{
    for(unsigned int i=0; i<this->getSize(); i++)
    {
        unsigned int l = 0, c = 0;
        Coord::coord2D(i, l, c);

        if (i == this->m_pos_player)
        {
            g.drawT(g.getSpritePlayer(this->m_dir_player), c, l);
        }
        else
        {
            g.drawT(g.getSprite(this->m_field[i]), c, l);
        }
    }
}
/*
int main()
{
    char choix, choix2;
    std::string path;


    do
    {
        system("cls");
        std::cout << "     SOKOBAN" << std::endl << std::endl;
        std::cout << " 1. Jouer" << std::endl;
        std::cout << " 2. Resolution automatique" << std::endl;
        std::cin >> choix;
        switch (choix)
        {
            case '1':   path=choixniveau();
                        if (path != "retour"){jouer(path);}

                        break;

            case '2':   system("cls");
                        std::cout << "     SOKOBAN" << std::endl << std::endl;
                        std::cout << " 1. Jouer" << std::endl;
                        std::cout << " 2. Resolution automatique" << std::endl;
                        std::cout << " \t1. Brute Force" << std::endl;
                        std::cout << " \t2. BFS" << std::endl;
                        std::cout << " \t3. DFS" << std::endl;
                        std::cin >> choix2;
                        switch (choix2)
                        {
                            case '1':   path=choixniveau();
                                        if (path != "retour"){bruteforce(path);}
                                        break;

                            case '2':
                            {
                                path=choixniveau();
                                if (path != "retour"){bfs(path);}
                                // Load level from a file
                                Maze m(path.c_str());
                            //    Maze m("./levels/easy/easy_10.txt");
                                if (!m.init()) return -1;
                                m.bfs();fond
                                break;
                            }

                            case '3':
                                        break;

                            default:    break;
                        }
                        break;

            default:    break;
        }
    }while ((choix != '3') && (choix != 'q'));

    return 0;
}
END_OF_MAIN()
*/

int main()
{
    Game jeu;

    if (g.init())
    {
        jeu.menu(g);
    }
    // Free memory
    g.deinit();
    return 0;
}
END_OF_MAIN()

